package com.example.cryptotask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(CryptoTaskApplication.class, args);
    }

}

package com.example.cryptotask.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Користувачі")
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ім'я")
    private String firstName;

    @Column(name = "прізвище")
    private String lastName;

    @Column(name = "електронна_пошта")
    private String email;

    @Column
    private String пароль;

    @Column
    private String адреса;

    @Column
    private String телефон;
}

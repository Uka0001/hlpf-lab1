package com.example.cryptotask.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Table(name = "Ордери")
@Setter
@Getter
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_користувача")
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_криптовалюти")
    private Cryptocurrency cryptocurrency;

    @Column(name = "тип")
    private String type;

    @Column(name = "кількість")
    private BigDecimal quantity;

    @Column(name = "ціна")
    private BigDecimal price;

    @Column(name = "статус")
    private String status;
}

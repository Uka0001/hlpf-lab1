package com.example.cryptotask.repository;

import com.example.cryptotask.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}

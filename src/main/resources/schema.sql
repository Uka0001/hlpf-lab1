-- Create the schema
-- Drop the database if it exists
DROP DATABASE IF EXISTS crypto_bd;
-- Create the database if it doesn't exist
CREATE DATABASE crypto_bd;

-- Create the Users table
CREATE TABLE crypto_bd."Користувачі" (
                                         id SERIAL PRIMARY KEY,
                                         "ім'я" VARCHAR(50),
                                         "прізвище" VARCHAR(50),
                                         "електронна_пошта" VARCHAR(100),
                                         "пароль" VARCHAR(100),
                                         "адреса" VARCHAR(100),
                                         "телефон" VARCHAR(20)
);

-- Create the Wallets table
CREATE TABLE crypto_bd."Кошельки" (
                                      id SERIAL PRIMARY KEY,
                                      id_користувача INT,
                                      "назва" VARCHAR(50),
                                      "баланс" DECIMAL(18, 2),
                                      FOREIGN KEY (id_користувача) REFERENCES crypto_bd."Користувачі"(id)
);

-- Create the Cryptocurrencies table
CREATE TABLE crypto_bd."Криптовалюти" (
                                          id SERIAL PRIMARY KEY,
                                          "назва" VARCHAR(50),
                                          "символ" VARCHAR(10)
);

-- Create the Orders table
CREATE TABLE crypto_bd."Ордери" (
                                    id SERIAL PRIMARY KEY,
                                    id_користувача INT,
                                    id_криптовалюти INT,
                                    "тип" VARCHAR(10),
                                    "кількість" DECIMAL(18, 8),
                                    "ціна" DECIMAL(18, 2),
                                    "статус" VARCHAR(20),
                                    FOREIGN KEY (id_користувача) REFERENCES crypto_bd."Користувачі"(id),
                                    FOREIGN KEY (id_криптовалюти) REFERENCES crypto_bd."Криптовалюти"(id)
);
